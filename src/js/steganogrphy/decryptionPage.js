import * as React from "react";
import {Fragment} from "react";
import Header from "../header";
import DecryptionForm from "./decryptionForm";


class DecryptionPage extends React.Component {

    render () {
        return (
            <Fragment>
                <Header page="decryption"/>
                <div className="form-container justify-content-center d-flex">
                    <DecryptionForm/>
                </div>
            </Fragment>
        );
    }

}

export default DecryptionPage;