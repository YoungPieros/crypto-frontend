import * as React from "react";
import {Fragment} from "react";
import Header from "../header";
import EncryptionForm from "./encryptionForm";


class EncryptionPage extends React.Component {

    render () {
        return (
            <Fragment>
                <Header page="encryption"/>
                <div className="form-container justify-content-center d-flex">
                    <EncryptionForm/>
                </div>
            </Fragment>
        );
    }

}

export default EncryptionPage;