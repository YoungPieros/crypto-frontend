import * as React from "react";
import {Fragment} from "react";
import SteganographyForm from "./steganographyForm";

class EncryptionForm extends SteganographyForm {

    constructor(props) {
        super(props);
        this.initialState = {
            inputValues: {text: null, password: null},
            validateInputFields: {image: null, text: null, password: null},
            image: null,
            loginButtonActivation: false,
            imagePreview: null,
            passwordVisibility: false,
            encodedImage: null, downloaderActivation: null, downloadMode: false
        };
        this.state = this.copy(this.initialState);
        this.downloadEncryptedFile = this.downloadEncryptedFile.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
        this.renderAfterLoginButtons = this.renderAfterLoginButtons.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
        this.renderTextArea = super.renderTextArea.bind(this);
        this.getTextareaClassActiveFields = super.getTextareaClassActiveFields.bind(this);
        this.renderUploadButton = this.renderUploadButton.bind(this);
    }
    
    downloadEncryptedFile () {
        if (!this.state.downloaderActivation)
            return;
        let downloader = document.createElement('a');
        downloader.href = this.state.encodedImage;
        downloader.download = "encrypted_" + this.state.image.name;
        downloader.click();
    }

    checkValidation () {
        let validations = {image: null, text: null, password: null};
        validations['password'] = this.checkPasswordValidation();
        validations['text'] = this.checkTextValidation();
        validations['image'] = this.state.image === null ? null : true;
        this.setState(() => ({validateInputFields: validations}));
    }

    uploadFile () {
        if (!this.state.loginButtonActivation)
            return;
        this.setState(() => ({downloadMode: true}));
        let form = new FormData();
        form.append('image', this.state.image, this.state.image.name);
        form.append('password', this.state.inputValues.password);
        form.append('text', this.state.inputValues.text);
        fetch('http://localhost:8080/steganography/encryption', {
            headers: {
                Authorization: localStorage.getItem("jwt")
            },
            method: 'POST',
            body: form})
            .then(response => {
                    response.blob().then(blob => {
                        let url = window.URL.createObjectURL(blob);
                        this.setState(() => ({encodedImage: url, downloaderActivation: true}));
                    })});
    }

    renderAfterLoginButtons () {
        return (
            <Fragment>
                <div className="col-md-6 justify-content-center d-flex float-left">
                    <button className="encrypt-btn disable-focus col-md-10 login-btn form-field rounded-pill text-white
                                                                   justify-content-center text-center no-border w-100 font-weight-bold reset-data-btn-failed"
                            onClick={this.resetData}>
                        <i className="fa align-self-stretch fa-2x fa-refresh float-left"/>
                        <span className="font-big">reset</span>
                    </button>
                </div>
                <div className="col-md-6 justify-content-center d-flex float-left">
                    <button className={"encrypt-btn disable-focus col-md-10 login-btn form-field rounded-pill text-white " +
                    "justify-content-center text-center no-border w-100 font-weight-bold " +
                    (this.state.downloaderActivation ? ' active' : ' not-active')}
                            onClick={this.downloadEncryptedFile}>
                        <i className="fa align-self-stretch fa-2x fa-cloud-download float-left"/>
                        <span className="font-big">download</span>
                    </button>
                </div>
            </Fragment>
        );
    }

    render() {
        return(
            <Fragment>
                <div className="d-flex align-content-center justify-content-center w-100">
                    <div className="d-flex justify-content-center align-items-center container align-content-center">
                        <div className="col-md-7 bg-transparent-dark encrypt-form">
                            {this.renderImageSelectorButton()}
                            {this.renderTextArea(false)}
                            {this.renderPasswordInput()}
                            <div  className="justify-content-center d-flex w-100">
                                {
                                    this.state.downloadMode ?
                                        <Fragment>
                                            {this.renderAfterLoginButtons()}
                                        </Fragment>

                                    :
                                        <Fragment>
                                            {this.renderUploadButton('encrypt')}
                                        </Fragment>
                                }
                            </div>
                        </div>
                    </div>
                </div>


            </Fragment>
        );
    }

}

export default EncryptionForm;