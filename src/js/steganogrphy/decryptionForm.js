import * as React from "react";
import {Fragment} from "react";
import SteganographyForm from "./steganographyForm";

class DecryptionForm extends SteganographyForm {

    constructor(props) {
        super(props);
        this.initialState = {
            inputValues: {password: null},
            validateInputFields: {image: null, password: null},
            decodedText: null, successfulDecoding: null,
            image: null,
            loginButtonActivation: false,
            imagePreview: null,
            passwordVisibility: false,
            decryptionMode: false
        }
        this.text = "";
        this.decryptionURI = "http://localhost:8080/steganography/decryption";
        this.state = this.copy(this.initialState);
        this.uploadFile = this.uploadFile.bind(this);
        this.renderAfterLoginButtons = this.renderAfterLoginButtons.bind(this);
        this.checkValidation = this.checkValidation.bind(this);
        this.renderTextArea = super.renderTextArea.bind(this);
        this.getTextareaClassActiveFields = this.getTextareaClassActiveFields.bind(this);
        this.renderUploadButton = this.renderUploadButton.bind(this);

    }

    checkValidation () {
        let validations = {image: null, password: null};
        validations['password'] = this.checkPasswordValidation();
        validations['image'] = this.state.image === null ? null : true;
        this.setState(() => ({validateInputFields: validations}));
    }

    uploadFile () {
        if (!this.state.loginButtonActivation)
            return;
        this.setState(() => ({decryptionMode: true}));
        let form = new FormData();
        form.append('image', this.state.image, this.state.image.name);
        form.append('password', this.state.inputValues.password);
        fetch(this.decryptionURI, {
                headers: {
                    Authorization: localStorage.getItem("jwt")
                },
                method: 'POST',
                body: form})
            .then(response => {
                if (response.status === 200)
                    response.text().then( (text) => {
                    this.setState(() => ({decodedText: text, successfulDecoding: true}));
                })
                else
                    response.json().then((data) => {
                        this.setState(() => ({decodedText: data.message, successfulDecoding: false}));
                    })
            })
    }

    getTextareaClassActiveFields () {
        let activation = this.state.decodedText;
        return activation === null ? "text-white" : (activation ? "green-color" : "darkred-color");
    }

    renderAfterLoginButtons () {
        return (
            <div className="col-md-6 justify-content-center d-flex float-left">
                <button className={"encrypt-btn disable-focus col-md-10 login-btn form-field rounded-pill text-white " +
                "justify-content-center text-center no-border w-100 font-weight-bold " +
                "reset-data-btn-" + (this.state.successfulDecoding ? "success" : "failed")}
                        onClick={this.resetData.bind(this)}>
                    <i className="fa align-self-stretch fa-2x fa-refresh float-left"/>
                    <span className="font-big">reset</span>
                </button>
            </div>
        );
    }

        render() {
        return(
            <Fragment>
                <div className="d-flex align-content-center justify-content-center w-100">
                    <div className="d-flex justify-content-center align-items-center container align-content-center">
                        <div className="col-md-7 bg-transparent-dark encrypt-form">
                            {this.renderImageSelectorButton()}
                            {this.renderTextArea(true)}
                            {this.renderPasswordInput()}
                            <div  className="justify-content-center d-flex w-100">
                                {
                                    this.state.decryptionMode ?
                                        <Fragment>
                                            {this.renderAfterLoginButtons()}
                                        </Fragment>
                                        :
                                        <Fragment>
                                            {this.renderUploadButton('decrypt')}
                                        </Fragment>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default DecryptionForm;