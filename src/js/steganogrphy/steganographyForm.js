import * as React from "react";
import Form from "../authorization/form";

class SteganographyForm extends Form {


    checkPasswordValidation () {
        let value = this.state.inputValues['password'];
        return value === null ? null : (value.length > 7);
    }

    checkTextValidation () {
        let text = this.state.inputValues['text'];
        return text === null ? null : (text !== "" && text.length < 1024);
    }

    getTextareaClassActiveFields () {
        let activation = this.state.validateInputFields['text'];
        return activation === null ? "text-white" : (activation ? "green-color" : "darkred-color");
    }

    getSelectImageClassActiveFields (name) {
        let activation = this.state.validateInputFields[name];
        return activation === null ? " white-border text-white" :
            (activation ? " green-color " : " darkred-color darkred-border ")
    }

    renderImageSelectorButton () {
        return (
            <div className="justify-content-center d-flex">
                <div className="d-flex flex-column justify-content-center">
                    <div className="image-preview col-md-5 d-flex justify-content-center container align-content-center">
                        {
                            this.state.imagePreview ?
                                <img
                                    className="image-preview w-100 h-100 bg-transparent-mid-light no-border img-thumbnail"
                                    src={this.state.imagePreview}
                                    alt="pre view file"/>
                                :
                                <i className="bg-transparent-mid-light text-white font-weight-light fa fa-file-photo-o fa-8x"/>
                        }
                    </div>
                    <div className="d-flex justify-content-center shadow-lg container">
                        <input type="file" id="files" onChange={this.changeFile} accept="image/png image/bmp" className="d-none"/>
                        <label htmlFor="files"
                               className={"btn bg-transparent-light text-white with-border " + this.getSelectImageClassActiveFields("image")}>
                            Select Image
                        </label>
                    </div>
                </div>
            </div>
        );
    }

    renderTextArea (readOnly) {
        return (
            <div className="justify-content-center d-flex row">
                <div className="col-md-10 row flex">
                    <div className="col-md-12">
                        <textarea value={this.state.decodedText === null ? "" : this.state.decodedText} placeholder="secret text"
                                  readOnly={readOnly}
                                  className={"bg-transparent-mid-light text-white col-md-12 font-italic " +
                                  "font-weight-bold img-thumbnail " + this.getTextareaClassActiveFields()} name="text" rows={8} onChange={this.changeField}>
                        </textarea>
                    </div>
                </div>
            </div>
        );
    }

    renderPasswordInput () {
        return (
            <div className="justify-content-center d-flex row">
                <div className="col-md-9 row d-flex justify-content-center">
                    <div className="col-md-10 d-flex justify-content-center">
                        <div className="col-md-12 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-lock input-field-icon " + this.getClassActiveFields("password")}/>
                            <input value={this.state.inputValues.password === null ? "" : this.state.inputValues.password} name={"password"} type={(this.state.passwordVisibility ? "text": "password")} required={true} placeholder={"password"}
                                   className="middle-placeholder bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                            <i
                                onClick={this.changePasswordVisibility} className={(this.state.passwordVisibility ? "fa-eye": "fa-eye-slash") + " align-items-center float d-flex fa fa-eye input-field-icon"}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderUploadButton (name) {
        return (
            <button className={"encrypt-btn disable-focus col-md-7 login-btn form-field rounded-pill text-white " +
            "justify-content-center text-center no-border w-100 active" +
            (this.state.loginButtonActivation ? ' active' : ' not-active')}
                    onClick={this.uploadFile}>
                <i className="fa align-self-stretch fa-2x fa-cloud-upload float-left"/>
                <span className="font-big">{name}</span>
            </button>
        );
    }

    render() {
        return("");
    }

}

export default SteganographyForm;