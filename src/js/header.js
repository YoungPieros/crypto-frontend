import * as React from "react";
import {Link} from "react-router-dom";
import {Fragment} from "react";


class Header extends React.Component {

    constructor(props) {
        super(props);
        this.classes = {
            register: "link not-active-link",
            login: "link not-active-link",
            aboutUs: "link not-active-link",
            encryption: "link not-active-link",
            decryption: "link not-active-link"
        }
    }

    render () {
        let currentPage = this.props.page;
        this.classes[currentPage] = "link active-link";
        return (
            <div className="navbar">
                <ul className="header-links">
                    {
                        !this.props.authorized &&
                        <Fragment>
                            <Link to="/Register" className={this.classes.register}>
                                Register
                            </Link>
                            <Link to={'/login'} className={this.classes.login}>
                                Login
                            </Link>
                        </Fragment>
                    }
                    <Link to={'/steganography/encryption'} className={this.classes.encryption}>
                        Encryption
                    </Link>
                    <Link to={'/steganography/decryption'} className={this.classes.decryption}>
                        Decryption
                    </Link>
                    <Link to={'/aboutUs'} className={this.classes.aboutUs}>
                        About Us
                    </Link>
                </ul>
            </div>
        );
    }

}

Header.defaultProps = {
    authorized: true
}


export default Header;