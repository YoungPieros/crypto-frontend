import * as React from "react";
import {Fragment} from "react";
import Header from "../header";
import LoginForm from "./LoginForm";


class Login extends React.Component {

    render () {
        return (
            <Fragment>
                <Header authorized={false} page="login"/>
                <div className="form-container justify-content-center d-flex">
                    <LoginForm/>
                </div>
            </Fragment>
        );
    }

}

export default Login;