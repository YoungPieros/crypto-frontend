import Form from "./form";
import googleLogo from "../../pics/google-logo.png";
import * as React from "react";
import {Link} from "react-router-dom";

class LoginForm extends Form {

    constructor(props) {
        super(props);
        this.loginAPI = "http://localhost:8080/user/login";
        this.state = {
            validateInputFields: {email: null, password: null},
            inputValues: {email: null, password: null},
            login: false,
            loginButtonActivation: false,
            loginData: null
        }
        this.loginAccount = this.loginAccount.bind(this);
    }

    checkValidation () {
        let validations = {email: null, password: null};
        validations['password'] = this.checkPasswordValidation();
        validations['email'] = this.checkEmailValidation();
        this.setState(() => ({validateInputFields: validations}));
    }

    checkPasswordValidation () {
        let value = this.state.inputValues['password'];
        return value === null ? null : (value.length >= 8);
    }

    buildRequest () {
        const Buffer = require("buffer").Buffer;
        let encodedLoginData = this.state.inputValues.email + ":" + this.state.inputValues.password;
        let encodedAuth = "Basic " + new Buffer(encodedLoginData).toString("base64");
        return {
            method: 'POST',
            headers: {
                // 'content-length' : queryString.length.toString(),
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Authorization': encodedAuth
            }
        };
    }

    loginAccount () {
        if (!this.state.loginButtonActivation)
            return;
        let request = this.buildRequest();
        fetch(this.loginAPI, request)
            .then(response => {
                return response.json();
            })
            .then(data => {
                this.setState(() => ({loginData: data}));
            });
        this.setState(() => ({signingUp: true}));
    }

    render() {
        if (this.state.loginData != null) {
            localStorage.removeItem("jwt");
            localStorage.setItem("jwt", this.state.loginData.type + " " + this.state.loginData.token);
        }
        this.checkLoginButtonActivation();
        return (
            <div className="container-login-form container justify-content-center text-center row">
                <div className="col-md-7 login-div bg-transparent-dark row justify-content-center align-items-center align-content-center">
                    <div className="row login-form col-md-12 justify-content-center d-flex">
                        <div className="col-md-9 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-envelope input-field-icon " + this.getClassActiveFields("email")}/>
                            <input name={"email"} type="email" required={true} placeholder={"email"}
                                   className="bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                        </div>
                        <div className="col-md-9 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-lock input-field-icon " + this.getClassActiveFields("password")}/>
                            <input name={"password"} type="password" required={true} placeholder={"password"}
                                   className="bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                        </div>
                        <button className={"disable-focus col-md-8 d-flex login-btn form-field rounded-pill text-white " +
                                           "justify-content-center text-center no-border w-100" +
                                           (this.state.loginButtonActivation ? ' active' : ' not-active')}
                                onClick={this.loginAccount}
                        >
                            Login
                        </button>

                    </div>
                    <div className="row login-form col-md-8 justify-content-center d-inline-block d-flex">

                        <button className="disable-focus disable-active-focus col-md-8 d-flex login-btn text-center align-items-center
                                           google-login-field rounded-pill no-border text-white">
                            <div className=" d-flex position-absolute w-100 justify-content-center">
                                Login With Google
                            </div>
                            <img src={googleLogo} className="d-flex google-logo"  alt={"google logo"}/>
                        </button>

                        <div className="row col-md-12 justify-content-center">
                            <a className="login-options" href={"/login"}>
                                forgot password?
                            </a>
                        </div>
                        <div className="row col-md-12 justify-content-center">
                            <Link to={'/register'} className="login-options">
                                register now
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

        );
    }

}

export default LoginForm;