import * as React from "react";
import {Fragment} from "react";
import Header from "../header";
import RegisterForm from "./RegisterForm";


class Register extends React.Component {

    render () {
        return (
            <Fragment>
                <Header authorized={false} page="register"/>
                <div className="form-container justify-content-center d-flex">
                    <RegisterForm/>
                </div>
            </Fragment>
        );
    }

}

export default Register;