import googleLogo from "../../pics/google-logo.png";
import * as React from "react";

class GoogleLoginButton extends React.Component {

    render() {
        return (
            <button className="disable-focus col-md-8 d-flex float-right login-btn align-items-center align-content-center
                                           google-login-field rounded-pill no-border text-white">
                <div className=" d-flex position-absolute w-100 justify-content-center">
                    Login With Google
                </div>
                <img src={googleLogo} className="google-logo float-left" alt={"google login"}/>
            </button>
        );
    }

}

export default GoogleLoginButton;