import Form from "./form";
import * as React from "react";
import {Link} from "react-router-dom";
import GoogleLoginButton from "./googleLoginButton";

class RegisterForm extends Form {

    constructor(props) {
        super(props);
        this.MIN_PASSWORD_LENGTH = 8;
        this.registerAPI = "http://localhost:8080/user/register";
        this.state = {
            validateInputFields: {firstName: null, lastName: null, email: null, password: null, confirmPassword: null},
            inputValues: {firstName: null, lastName: null, email: null, password: null, confirmPassword: null},
            registered: false,
            loginButtonActivation: false
        }
        this.changeField = super.changeField.bind(this);
        this.registerAccount = this.registerAccount.bind(this);
    }

    checkValidation () {
        let validations = {firstName: null, lastName: null, email: null, password: null, confirmPassword: null};
        validations['firstName'] = this.checkNameValidation('firstName');
        validations['lastName'] = this.checkNameValidation('lastName');
        validations['password'] = this.checkPasswordValidation();
        validations['confirmPassword'] = this.checkConfirmPasswordValidation();
        validations['email'] = this.checkEmailValidation();
        this.setState(() => ({validateInputFields: validations}));
    }

    checkNameValidation (field) {
        let value = this.state.inputValues[field];
        return value === null ? null : (value !== "");
    }

    checkPasswordValidation () {
        let value = this.state.inputValues['password'];
        return value === null ? null : (value.length >= this.MIN_PASSWORD_LENGTH);
    }

    checkConfirmPasswordValidation () {
        let value = this.state.inputValues['confirmPassword'];
        let password = this.state.inputValues['password'];
        return (value === null || password === null || password === "") ? null : (value === password);
    }

    buildRequest () {
        let params = {  "firstName": this.state.inputValues.firstName,
            "lastName": this.state.inputValues.lastName,
            "password": this.state.inputValues.password ,
            "email": this.state.inputValues.email };
        let queryString = Object.keys(params).map(function(key) {
            return key + '=' + params[key]
        }).join('&');
        return  {
            method: 'POST',
            headers: {
                'content-length' : queryString.length.toString(),
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: queryString
        };
    }

    registerAccount () {
        if (!this.state.loginButtonActivation)
            return;
        let request = this.buildRequest();
        fetch(this.registerAPI, request)
            .then(response => {
                return response.json();})
            .then(data => {
            });
        this.setState(() => ({signingUp: true}));
    }

    render() {
        return (
            <div className="container-login-form container align-self-auto justify-content-center text-center row">
                <div className="col-md-8 login-div bg-transparent-dark row justify-content-center align-items-center align-content-center">
                    <div className="row login-form col-md-12 justify-content-center d-flex">
                        <div className="col-md-5 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-user input-field-icon " + this.getClassActiveFields("firstName")}/>
                            <input name={"firstName"} type="text" required={true} placeholder={"first name"}
                                   className="bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                        </div>
                        <div className="col-md-5 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-user input-field-icon " + this.getClassActiveFields("lastName")}/>
                            <input name={"lastName"} type="text" required={true} placeholder={"last name"}
                                   className="bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                        </div>
                        <div className="col-md-10 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-envelope input-field-icon " + this.getClassActiveFields("email")}/>
                            <input name={"email"} type="email" required={true} placeholder={"email"}
                                   className="bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                        </div>
                        <div className="col-md-5 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-lock input-field-icon " + this.getClassActiveFields("password")}/>
                            <input name={"password"} type="password" required={true} placeholder={"password"}
                                   className="bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                        </div>
                        <div className="col-md-5 d-flex form-field rounded-pill text-white">
                            <i className={"align-items-center d-flex fa fa-lock input-field-icon " + this.getClassActiveFields("confirmPassword")}/>
                            <input name={"confirmPassword"} type="password" required={true} placeholder={"confirm password"}
                                   className="bg-transparent w-100 input-field no-border text-white font-weight-bold"
                                   onChange={this.changeField}
                            />
                        </div>
                        <button
                            className={"disable-focus col-md-7 d-flex login-btn form-field " +
                                       "rounded-pill text-white no-border justify-content-center" +
                                        (this.state.loginButtonActivation ? ' active' : ' not-active')}
                                onClick={this.registerAccount}>
                            Login
                        </button>

                    </div>
                    <div className="row login-form col-md-10 justify-content-center d-inline-block d-flex">

                        <GoogleLoginButton/>
                        <div className="row col-md-12 justify-content-center">
                            <Link to={'/login'} className="login-options">
                                Already Have Account
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

        );
    }

}

export default RegisterForm;