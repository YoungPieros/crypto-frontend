import * as React from "react";
import Utils from "../../utils/Utils";

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.resetData = this.resetData.bind(this);
        this.changeField = this.changeField.bind(this);
        this.changeFile = this.changeFile.bind(this);
        this.changePasswordVisibility = this.changePasswordVisibility.bind(this);
    }

    async changeField (event) {
        let field = event.target.name;
        let value = event.target.value;
        let inputValues = this.state.inputValues;
        inputValues[field] = value;
        await this.setState(() => ({inputValues: inputValues}));
        await this.checkValidation();
        this.checkLoginButtonActivation();
    }

    copy(data) {
        return JSON.parse(JSON.stringify(data));
    }

    async resetData () {
        let initialState = this.copy(this.initialState);
        this.setState(() => initialState);
    }

    async changeFile (event) {
        let file = event.target.files[0];
        await this.setState(() => (
            {image: file, imagePreview: URL.createObjectURL(file)}))
        this.checkValidation();
        this.checkLoginButtonActivation();
    }

    changePasswordVisibility() {
        this.setState(() => ({passwordVisibility: !this.state.passwordVisibility}));
    }


    checkValidation () {
    }

    checkEmailValidation () {
        let email = this.state.inputValues['email'];
        return email === null ? null : Utils.validateEmail(email);
    }

    checkLoginButtonActivation () {
        let validations = this.state.validateInputFields;
        let registerActivation = true;
        for (let key in validations)
            if (validations[key] !== true) {
                registerActivation = false;
                break;
            }
        if (this.state.loginButtonActivation !== registerActivation)
            this.setState(() => ({loginButtonActivation: registerActivation}));
    }

    getClassActiveFields (name) {
        let activation = this.state.validateInputFields[name];
        return activation === null ? "" : (activation ? "green-color" : "darkred-color");
    }


    render() {
        return ("");
    }


}

export default Form;