import React from 'react';
import './App.css';
import Login from "./js/authorization/login";
import {BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Register from "./js/authorization/register";
import EncryptionPage from "./js/steganogrphy/encryptionPage";
import DecryptionPage from "./js/steganogrphy/decryptionPage";


function App() {
        return (
            <Router>
                <Switch>

                    <Route path={"/login"}>
                        <Login/>
                    </Route>
                    <Route path="/register">
                        <Register/>
                    </Route>

                    <Route path="/steganography/decryption">
                        <DecryptionPage/>
                    </Route>
                    <Route path={"/steganography/encryption"}>
                        <EncryptionPage/>
                    </Route>

                    <Route path="/">
                        <Login/>
                    </Route>
                </Switch>
            </Router>
        );
}

export default App;
